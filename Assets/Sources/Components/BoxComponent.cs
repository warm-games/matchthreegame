﻿using Leopotam.Ecs;
using MatchThreeGame.Views;
using UnityEngine;

namespace MatchThreeGame.Components
{
    public class BoxComponent : IEcsAutoReset
    {
        public BoxView BoxView { get; set; }

        public Vector2Int BoardPosition { get; set; }

        public void Reset()
        {
            BoxView.OnDropCallback = null;
            BoxView = null;
        }
    }
}

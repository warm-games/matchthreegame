﻿using Leopotam.Ecs;
using UnityEngine;

namespace MatchThreeGame.Components
{
    public class BoxSwapEvent : IEcsOneFrame
    {
        public Vector2Int First;
        public Vector2Int Second;
    }
}

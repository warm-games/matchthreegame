﻿using MatchThreeGame.Views;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace MatchThreeGame.Pools
{
    public class BoxesPool : MonoMemoryPool<BoxView>
    {
        protected readonly GameObject _prefab;

        public BoxesPool(GameConfig config)
        {
            _prefab = config.BoxPrefab;
        }

        protected override void OnCreated(BoxView item)
        {
            item.SpriteRenderer = item.GetComponent<SpriteRenderer>();
            item.Collider = item.GetComponent<BoxCollider2D>();
            item.TextField = item.GetComponentInChildren<Text>();
            item.gameObject.SetActive(false);

            base.OnCreated(item);
        }

        protected override void OnSpawned(BoxView item)
        {
            item.gameObject.SetActive(true);
            item.TextField.text = "";

            base.OnSpawned(item);
        }

        protected override void OnDespawned(BoxView item)
        {
            item.gameObject.SetActive(false);

            base.OnDespawned(item);
        }
    }
}

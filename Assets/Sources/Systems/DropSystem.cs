﻿using Leopotam.Ecs;
using MatchThreeGame.Components;
using System.Collections.Generic;
using UnityEngine;

namespace MatchThreeGame.Systems
{
    public class DropSystem : IEcsRunSystem
    {
        protected readonly EcsFilter<BoxComponent, MatchedTag> _matchedBoxes;

        protected readonly GameContext _context;
        protected readonly GameConfig _config;

        protected readonly HashSet<EcsEntity> _updatedEntites;

        public DropSystem(GameContext context, GameConfig config)
        {
            _context = context;
            _config = config;

            _updatedEntites = new HashSet<EcsEntity>();
        }

        public void Run()
        {
            foreach (var i in _matchedBoxes)
            {
                var boxComponent = _matchedBoxes.Get1[i];

                UpdateDropCount(boxComponent.BoardPosition);
            }

            if (_updatedEntites.Count > 0)
            {
                foreach (var entity in _updatedEntites)
                {
                    var boxComponent = entity.Get<BoxComponent>();
                    var dropComponent = entity.Get<DropComponent>();

                    var boardPosition = new Vector2Int(boxComponent.BoardPosition.x - dropComponent.DropCount * _context.Gravity.x,
                        boxComponent.BoardPosition.y - dropComponent.DropCount * _context.Gravity.y);

                    dropComponent.DropCount = 0;

                    boxComponent.BoardPosition = boardPosition;

                    _context.GameBoard[boardPosition.x, boardPosition.y] = entity;

                    entity.Set<MoveTag>();
                }

                _updatedEntites.Clear();
            }
        }

        protected void UpdateDropCount(Vector2Int boardPosition)
        {
            if (_context.Gravity.x != 0)
            {
                for (var col = boardPosition.x; col >= 0 && col < _config.Cols; col += _context.Gravity.x)
                    UpdateEntity(new Vector2Int(col, boardPosition.y));
            }
            else if (_context.Gravity.y != 0)
            {
                for (var row = boardPosition.y; row >= 0 && row < _config.Rows; row += _context.Gravity.y)
                    UpdateEntity(new Vector2Int(boardPosition.x, row));
            }
        }

        protected void UpdateEntity(Vector2Int boardPosition)
        {
            var entity = _context.GetGameBoardEntity(boardPosition);

            if (entity.IsNull()) return;

            if (entity.Get<MatchedTag>() != null) return;

            _updatedEntites.Add(entity);

            var dropComponent = entity.Get<DropComponent>();

            ++dropComponent.DropCount;
        }
    }
}

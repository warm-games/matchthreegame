﻿using DG.Tweening;
using Leopotam.Ecs;
using MatchThreeGame.Components;
using MatchThreeGame.Utils;

namespace MatchThreeGame.Systems
{
    public class MoveSystem : IEcsRunSystem
    {
        protected readonly EcsFilter<BoxComponent, MoveTag> _moveBoxes;

        protected readonly GameContext _context;
        protected readonly GameConfig _config;

        public MoveSystem(GameConfig config, GameContext context)
        {
            _config = config;
            _context = context;
        }

        public void Run()
        {
            if (_context.State != GameContext.GameState.DestroyCompleted) return;

            var moving = _moveBoxes.GetEntitiesCount() > 0;

            foreach (var i in _moveBoxes)
            {
                var entity = _moveBoxes.Entities[i];
                var boxComponent = _moveBoxes.Get1[i];
                var worldPosition = CoordinatesHelper.BoardToWorldPosition(boxComponent.BoardPosition, _context.BoxSize, _context.HalfBoxSize);

                entity.Unset<MoveTag>();

                boxComponent.BoxView.transform.DOMove(worldPosition, _config.DefaultAnimationTime)
                    .OnComplete(() => entity.Set<MovedTag>());
            }

            if (moving)
            {
                _context.State = GameContext.GameState.Move;

                var timer = DOTween.Sequence();
                timer.AppendInterval(_config.DefaultAnimationTime);
                timer.AppendCallback(() => _context.State = GameContext.GameState.Idle);
            }
        }
    }
}

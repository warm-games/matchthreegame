﻿using DG.Tweening;
using Leopotam.Ecs;
using MatchThreeGame.Components;
using MatchThreeGame.Pools;
using UnityEngine;

namespace MatchThreeGame.Systems
{
    public class DestroySystem : IEcsRunSystem
    {
        protected readonly EcsFilter<BoxComponent, MatchedTag> _matchedBoxes;
        protected readonly EcsFilter<MatchedTag, GravityBombTag> _gravityBoms;

        protected readonly GameConfig _config;
        protected readonly GameContext _context;

        protected readonly BoxesPool _boxesPool;

        protected readonly EcsWorld _world;

        public DestroySystem(GameConfig config, GameContext context, BoxesPool boxesPool, EcsWorld world)
        {
            _config = config;
            _context = context;
            _boxesPool = boxesPool;
            _world = world;
        }

        public void Run()
        {
            if (_context.State != GameContext.GameState.Idle) return;

            foreach (var i in _matchedBoxes)
            {
                var entity = _matchedBoxes.Entities[i];
                var boxComponent = entity.Get<BoxComponent>();

                entity.Unset<MovedTag>();
                entity.Unset<MoveTag>();

                ++_context.DestroyedBoxesInCol[boxComponent.BoardPosition.x];
                ++_context.DestroyedBoxesInRow[boxComponent.BoardPosition.y];

                _world.NewEntityWith<SpawnBoxEvent>(out var spawnBoxEvent);
                spawnBoxEvent.BoardPosition = boxComponent.BoardPosition;                    

                boxComponent.BoxView.transform
                    .DOScale(0f, _config.DefaultAnimationTime)
                    .OnComplete(() =>
                    {
                        _boxesPool.Despawn(boxComponent.BoxView);

                        entity.Destroy();
                    });
            }

            foreach(var i in _gravityBoms)
                _world.NewEntityWith<GravityBombEvent>(out _);

            if (_matchedBoxes.GetEntitiesCount() > 0)
            {
                _context.State = GameContext.GameState.Destroy;

                var timer = DOTween.Sequence();
                timer.AppendInterval(_config.DefaultAnimationTime);
                timer.AppendCallback(() => _context.State = GameContext.GameState.DestroyCompleted);
            }
        }
    }
}
